package solver;

import ttp.TTP1Instance;
import ttp.TTPSolution;
import utils.Deb;
import utils.InsertionHelper;
import utils.ThreeOptHelper;

import java.util.ArrayList;


/**
 * local search algorithms 
 * based on TSP's 3-Opt and KP's Bit-flip
 * 
 * @author elkrari
 *
 */
public class J3B extends LocalSearch {


  public J3B() {
    super();
  }

  public J3B(TTP1Instance ttp) {
    super(ttp);
  }
  

  

  // search heuristic
  @Override
  public TTPSolution search() {

    //===============================================
    // generate initial solution
    //===============================================
    Constructive construct = new Constructive(ttp);

    if (s0==null) {
      s0 = new TTPSolution(
        construct.randomTour(),
        construct.randomPickingPlan()
      );
    }

    //ttp.objective(s0);
    //Deb.echo("STARTING SOL >> "+s0.ob);
    //===============================================

    // calculate initial objective value
    ttp.objective(s0);
//    Deb.echo(s0.ob);
    // copy initial solution into improved solution
    TTPSolution sol = s0.clone();//, sBest = s0.clone();
    
    // TTP data
    int nbCities = ttp.getNbCities();
    int nbItems = ttp.getNbItems();
    int[] A = ttp.getAvailability();
    
    // initial solution data
    int[] tour = sol.getTour();
    int[] pickingPlan = sol.getPickingPlan();

    // improvement indicator
    boolean improv;
    
    // best solution
    int xBest=0, yBest=0, zBest=0, kBest=0, oBest=0;
    double GBest = sol.ob;

    int[] pos = new int[nbCities+1];      // city/index store

    // intermediate solution
    int[] it, ipp;
    TTPSolution isol;
    

    int x, y, z, k;
    int nbIter = 0;

    do {
      /* map indices to their associated cities */
      for (int q=0; q<nbCities; q++) { // @todo move this to solution coding?
        pos[tour[q]] = q;
      }
      
      improv = false;
      nbIter++;
      
      // TSP 3-Opt
      for (x = 0; x <= nbCities - 4; x++) {
        for (y = x + 2; (x == 0 && y < nbCities - 3) || (x > 0 && y < nbCities - 2); y++) {
          for (z = y + 2; (x == 0 && z < nbCities - 1) || (x > 0 && z < nbCities); z++) {


          /* ****************************** */
          /* one bit flip                   */
          /* ****************************** */
          for (k=0; k<nbItems; k++) {
            
            /* cleanup and stop execution */
            if (Thread.currentThread().isInterrupted()) {
              return sol;
            }
            
            /* check if new weight doesn't exceed knapsack capacity */
            if (pickingPlan[k]==0 && ttp.weightOf(k)>sol.wend) {
              continue;
            }

            isol = sol.clone();
            ipp = isol.getPickingPlan();
            ipp[k] = ipp[k] != 0 ? 0 : A[k];

            /*----- Four possibilities how to change a tour with a 3-Opt -----*/
            for(int o=1;o<=4;o++) {
            /*----- Apply 3-Opt and Bit Flip Movements -----*/
              it = ThreeOptHelper.do3Opt(tour, pos, x, y, z, o);
              isol.setTour(it);
            /*----- Calculate Neighbor's Objective Value -----*/
              ttp.objective(isol);
              if (sol.ob > GBest) {
                xBest = x;
                yBest = y;
                zBest = z;
                oBest = o;
                kBest = k;
                GBest = sol.ob;
                improv = true;

                if (firstfit) break;
              }
            }
            if (firstfit) break;

          } // END FOR k
          if (firstfit && improv) break;
          } // END FOR z
          if (firstfit && improv) break;
        } // END FOR y
        if (firstfit && improv) break;
      } // END FOR x

      /*
       * test improvement
       */
      if (improv) {
        
        // 3-Opt
        sol.setTour(ThreeOptHelper.do3Opt(tour, pos, xBest, yBest, zBest, oBest));
        // bit-flip
        pickingPlan[kBest] = pickingPlan[kBest]!=0 ? 0 : A[kBest];
        
        ttp.objective(sol);

        // debug print
        if (this.debug) {
          Deb.echo("Best "+nbIter+":");
          Deb.echo(sol);
          Deb.echo("ob-best: "+sol.ob);
          Deb.echo("wend   : "+sol.wend);
          Deb.echo("---");
        }
      }
      
    } while(improv);
    
    return sol;
  }



  // returns the J3B neighborhood for a given solution
  @Override
  public ArrayList<TTPSolution> neighborhood(TTPSolution sol) {
    ArrayList<TTPSolution> neig = new ArrayList<>();
    int[] tour = sol.getTour();
    int[] pp = sol.getPickingPlan();
    int nbCities = tour.length;
    int nbItems = pp.length;
    int x,y,z,k,o;
    int[] A = ttp.getAvailability();

    for (x = 0; x <= nbCities - 4; x++) {
      for (y = x + 2; (x == 0 && y < nbCities - 3) || (x > 0 && y < nbCities - 2); y++) {
        for (z = y + 2; (x == 0 && z < nbCities - 1) || (x > 0 && z < nbCities); z++) {
          for(o=1;o<=4;o++) {
            for (k = 0; k < nbItems; k++) {
              // mutate tour
              int[] ntour = new int[nbCities];
              System.arraycopy(tour, 0, ntour, 0, nbCities);
              ThreeOptHelper.do3Opt(ntour, x, y, z, o);
              // mutate pick plan
              int[] npp = new int[nbItems];
              System.arraycopy(pp, 0, npp, 0, nbItems);
              npp[k] = pp[k] != 0 ? 0 : A[k];
              // make a neighbor solution & add in list
              TTPSolution nsol = new TTPSolution(ntour, npp);
              //ttp.objective(nsol);
              long cmpNsol = Long.parseLong(nsol.xCompressedString());
              nsol.index = ttp.enumerator.indices.get(cmpNsol);
              //Deb.echo(cmpNsol+" IDX="+nsol.index);
              neig.add(nsol);
            }
          }
        }
      }
    }
    // TODO: should we also consider solutions where tour/pick-plan is unchanged?

    return neig;
  }



}
