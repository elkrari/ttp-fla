package solver;

import ttp.TTP1Instance;
import ttp.TTPSolution;
import utils.Deb;
import utils.InsertionHelper;

import java.util.ArrayList;


/**
 * local search algorithms 
 * based on TSP's Insertion and KP's Bit-flip
 * 
 * @author elkrari
 *
 */
public class JIB extends LocalSearch {


  public JIB() {
    super();
  }

  public JIB(TTP1Instance ttp) {
    super(ttp);
  }
  

  

  // search heuristic
  @Override
  public TTPSolution search() {

    //===============================================
    // generate initial solution
    //===============================================
    Constructive construct = new Constructive(ttp);

    if (s0==null) {
      s0 = new TTPSolution(
        construct.randomTour(),
        construct.randomPickingPlan()
      );
    }

    //ttp.objective(s0);
    //Deb.echo("STARTING SOL >> "+s0.ob);
    //===============================================

    // calculate initial objective value
    ttp.objective(s0);
//    Deb.echo(s0.ob);
    // copy initial solution into improved solution
    TTPSolution sol = s0.clone();//, sBest = s0.clone();
    
    // TTP data
    int nbCities = ttp.getNbCities();
    int nbItems = ttp.getNbItems();
    int[] A = ttp.getAvailability();
    
    // initial solution data
    int[] tour = sol.getTour();
    int[] pickingPlan = sol.getPickingPlan();

    // improvement indicator
    boolean improv;
    
    // best solution
    int iBest=0, jBest=0, kBest=0;
    double GBest = sol.ob;
    

    int i, j, k;
    int nbIter = 0;

    do {
      
      improv = false;
      nbIter++;
      
      // TSP Insertion
      for (i=1; i<nbCities; i++) {
        for (j=1; j<nbCities; j++) {
          if(i==j)
              continue;


          /* ****************************** */
          /* one bit flip                   */
          /* ****************************** */
          for (k=0; k<nbItems; k++) {
            
            /* cleanup and stop execution */
            if (Thread.currentThread().isInterrupted()) {
              return sol;
            }
            
            /* check if new weight doesn't exceed knapsack capacity */
            if (pickingPlan[k]==0 && ttp.weightOf(k)>sol.wend) {
              continue;
            }

            /*----- Apply Insertion and Bit Flip Movements -----*/
            InsertionHelper.doInsertion(tour,i,j);
            pickingPlan[k] = pickingPlan[k]!=0 ? 0 : A[k];

            /*----- Calculate Neighbor's Objective Value -----*/
            ttp.objective(sol);

            /*----- Revert Changes -----*/
            InsertionHelper.doInsertion(tour,j,i);
            pickingPlan[k] = pickingPlan[k]!=0 ? 0 : A[k];

            // update best
            if (sol.ob > GBest) {
              
              iBest = i;
              jBest = j;
              kBest = k;
              GBest = sol.ob;
              improv = true;
              
              if (firstfit) break;
            }
            
          } // END FOR k
          if (firstfit && improv) break;
        } // END FOR j
        if (firstfit && improv) break;
      } // END FOR i

      /*
       * test improvement
       */
      if (improv) {
        
        // Insertion
        InsertionHelper.doInsertion(tour,iBest,jBest);
        // bit-flip
        pickingPlan[kBest] = pickingPlan[kBest]!=0 ? 0 : A[kBest];
        
        ttp.objective(sol);

        // debug print
        if (this.debug) {
          Deb.echo("Best "+nbIter+":");
          Deb.echo(sol);
          Deb.echo("ob-best: "+sol.ob);
          Deb.echo("wend   : "+sol.wend);
          Deb.echo("---");
        }
      }
      
    } while(improv);
    
    return sol;
  }



  // returns the JIB neighborhood for a given solution
  @Override
  public ArrayList<TTPSolution> neighborhood(TTPSolution sol) {
    ArrayList<TTPSolution> neig = new ArrayList<>();
    int[] tour = sol.getTour();
    int[] pp = sol.getPickingPlan();
    int nbCities = tour.length;
    int nbItems = pp.length;
    int i,j,k;
    int[] A = ttp.getAvailability();

    for (i=1; i<nbCities; i++) {
      for (j =1; j < nbCities; j++) {
        if(j==i)
          continue;
        for (k=0; k<nbItems; k++) {
          // mutate tour
          int[] ntour = new int[nbCities];
          System.arraycopy( tour, 0, ntour, 0, nbCities);
          InsertionHelper.doInsertion(ntour,i,j);
          // mutate pick plan
          int[] npp = new int[nbItems];
          System.arraycopy( pp, 0, npp, 0, nbItems);
          npp[k] = pp[k]!=0 ? 0 : A[k];
          // make a neighbor solution & add in list
          TTPSolution nsol = new TTPSolution(ntour,npp);
          //ttp.objective(nsol);
          long cmpNsol = Long.parseLong(nsol.xCompressedString());
          nsol.index = ttp.enumerator.indices.get(cmpNsol);
          //Deb.echo(cmpNsol+" IDX="+nsol.index);
          neig.add(nsol);
        }
      }
    }
    // TODO: should we also consider solutions where tour/pick-plan is unchanged?

    return neig;
  }



}
