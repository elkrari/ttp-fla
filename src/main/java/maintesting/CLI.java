package maintesting;

import enumerations.LON;
import enumerations.TTPEnumerator;
import generator.TTP1Generator;
import solver.J2B;
import solver.JIB;
import solver.LocalSearch;
import ttp.TTP1Instance;
import utils.Deb;

import java.io.FileNotFoundException;

/**
 * Created by yafrani on 10/21/17.
 */
public class CLI {

  public static void main(String[] args) throws FileNotFoundException {

    if (args.length < 5) {
      Deb.echo("Please specify all 5 parameters!");
      Deb.echo("e.g. java -jar ttp-lon-1.0.jar j2b 5 1 u 5");
      return;
    }

    // algorithm name
    final String algo = args[0];
    final int nbCities = Integer.parseInt(args[1]);
    final int nbItemsPerCity = Integer.parseInt(args[2]);
    final String correlation = args[3];
    final int capacityClass = Integer.parseInt(args[4]);

    TTP1Instance ttp = new TTP1Generator().generate(nbCities,
     100,200,
      correlation, nbItemsPerCity, capacityClass);
    Deb.echo(">> "+ttp.getName());

    //================================================
    // enumerate
    //================================================
    TTPEnumerator ttpEnumerator = new TTPEnumerator();

    ttpEnumerator.enumerate(ttp);
    ttp.enumerator = ttpEnumerator;

    //================================================
    // LS testing
    //================================================
    LocalSearch ls;
    switch (algo) {
      case "jib":
        ls = new JIB(ttp);
      break;

      case "j2b":
        ls = new J2B(ttp);
      break;

      default:
        ls = new J2B(ttp);
      break;
    }


    //================================================
    // store basins of attraction
    // (local optima and their associated solutions)
    //================================================
    long startTime, stopTime, exTime;

    LON lon = new LON(ttp, ttpEnumerator.solutions, ls);

    // load neighborhood
    Deb.echo("<load neighborhood matrix>");
    startTime = System.currentTimeMillis();
    lon.loadNeighborhood();
    stopTime = System.currentTimeMillis();
    exTime = stopTime - startTime;
    Deb.echo("Finished in "+(exTime/1000.0)+" sec");

    // load basins
    Deb.echo("<load basins>");
    startTime = System.currentTimeMillis();
    lon.loadBasins();
    stopTime = System.currentTimeMillis();
    exTime = stopTime - startTime;
    Deb.echo("Finished in "+(exTime/1000.0)+" sec");

    // connect basins
    Deb.echo("<connect basins>");
    startTime = System.currentTimeMillis();
    lon.connectBasins();
    stopTime = System.currentTimeMillis();
    exTime = stopTime - startTime;
    Deb.echo("Finished in "+(exTime/1000.0)+" sec");

    lon.store("output/"+ttp.getName()+".lon.txt");

    Deb.echo();
  }
}
