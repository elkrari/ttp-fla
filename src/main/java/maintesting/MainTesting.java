package maintesting;

import enumerations.LON;
import enumerations.TTPEnumerator;
import generator.TTP1Generator;
import solver.J2B;
import solver.JIB;
import solver.LocalSearch;
import ttp.TTP1Instance;
import ttp.TTPSolution;
import utils.Deb;

import java.io.FileNotFoundException;

/**
 * Created by yafrani on 10/21/17.
 */
public class MainTesting {

  public static void main(String[] args) throws FileNotFoundException {

//    TTP1Instance ttp = new TTP1Instance("eil4_n3_bounded-strongly-corr_05.ttp");
//    TTP1Instance ttp = new TTP1Instance("eil5_n4_bounded-strongly-corr_05.ttp");
//    TTP1Instance ttp = new TTP1Instance("eil6_n5_bounded-strongly-corr_05.ttp");
//    TTP1Instance ttp = new TTP1Instance("eil7_n6_bounded-strongly-corr_05.ttp");

    TTP1Instance ttp = new TTP1Generator().generate(5,100,200,
            "u",1,5);
    Deb.echo(ttp);

    Deb.echo("======================");


//    int m = ttp.getNbCities();
//    int n = ttp.getNbItems();


    Deb.echo("======================");

    //================================================
    // enumerate
    //================================================
    Deb.echo("======================");
    Deb.echo("== Enumerate all    ==");
    Deb.echo("======================");

    TTPEnumerator ttpEnumerator = new TTPEnumerator();

    ttpEnumerator.enumerate(ttp);
    ttp.enumerator = ttpEnumerator;
    Deb.echo("#solutions: "+ttp.enumerator.solutions.size());
    Deb.echo("=> Done !");
    Deb.echo("======================");

    //================================================
    // LS testing
    //================================================
    Deb.echo("======================");
    Deb.echo("== LS test          ==");
    Deb.echo("======================");

    LocalSearch ls = new JIB(ttp);

//    TTPSolution sol = new TTPSolution(new int[]{1,2,3,4,5},
//      new int[]{2,3,0,5});
//    Deb.echo(sol);
//    Deb.echo("======================");
//    ttp.objective(sol);
//    ls.neighborhood(sol);
//    Deb.echo("init obj: "+sol.ob);
//    ls.bestfit();
//    ls.debug();
//    ls.search();

    Deb.echo("======================");

//    //================================================
//    // basic code to obtain all local optima
//    //================================================
//    Deb.echo("== LS on all        ==");
//    ls.noDebug();
//    TreeSet<Long> basins = new TreeSet<>();
//    TTPSolution lo;
//    for (int i=0; i<ttpEnumerator.solutions.size(); i++) {
//      ls.setS0(ttpEnumerator.solutions.get(i));
//      lo = ls.search();
//      ttp.objective(lo);
//      long C = Long.parseLong(lo.xCompressedString());
//      basins.add(C);
//    }
//
//    Deb.echo("======================");
//    // print if small set...
//    if (basins.size()<100) {
//      for (long x : basins) {
//        TTPSolution y = new TTPSolution(m, n, x);
//        //Deb.echo(y);
//        ttp.objective(y);
//
//        Deb.echol(x);
//        Deb.echo(" " + y.fitness);
//      }
//    }



    //================================================
    // store basins of attraction
    // (local optima and their associated solutions)
    //================================================
    long startTime, stopTime, exTime;
    Deb.echo("======================");
    Deb.echo("== Local optima net ==");
    Deb.echo("======================");

    LON lon = new LON(ttp, ttpEnumerator.solutions, ls);

    // load neighborhood
    Deb.echo("<load neighborhood matrix>");
    startTime = System.currentTimeMillis();
    lon.loadNeighborhood();
    stopTime = System.currentTimeMillis();
    exTime = stopTime - startTime;
    Deb.echo("Finished in "+(exTime/1000.0)+" sec");

    // load basins
    Deb.echo("<load basins>");
    startTime = System.currentTimeMillis();
    lon.loadBasins();
    stopTime = System.currentTimeMillis();
    exTime = stopTime - startTime;
    Deb.echo("Finished in "+(exTime/1000.0)+" sec");

    // connect basins
    Deb.echo("<connect basins>");
    startTime = System.currentTimeMillis();
    lon.connectBasins();
    stopTime = System.currentTimeMillis();
    exTime = stopTime - startTime;
    Deb.echo("Finished in "+(exTime/1000.0)+" sec");

    Deb.echo(lon);
    lon.store("output/LON.txt");
    lon.storeBasins("output/Basins.txt");

    Deb.echo("======================");
    Deb.echo("======================");
    Deb.echo("======================");


    //================================================
    // Some statistics
    //================================================
    Deb.echo("======================");
    Deb.echo("== Statistics       ==");
    Deb.echo("======================");

    Deb.echo("# solutions  : "+ttpEnumerator.solutions.size());
    Deb.echo("# local opts : "+lon.basins.keySet().size());

    Deb.echo("======================");


  }
}

