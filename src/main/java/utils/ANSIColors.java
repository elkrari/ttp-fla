package utils;

/**
 * Created by yafrani on 8/3/17.
 */
public class ANSIColors {
  public static final String ANSI_RESET = "\u001B[0m";
  public static final String ANSI_BLACK = "\u001B[30m";
  public static final String ANSI_RED = "\u001B[31m";
  public static final String ANSI_GREEN = "\u001B[32m";
  public static final String ANSI_YELLOW = "\u001B[33m";
  public static final String ANSI_BLUE = "\u001B[34m";
  public static final String ANSI_PURPLE = "\u001B[35m";
  public static final String ANSI_CYAN = "\u001B[36m";
  public static final String ANSI_WHITE = "\u001B[37m";

  public static final String ANSI_B_BLACK = "\u001B[40m";
  public static final String ANSI_B_RED = "\u001B[41m";
  public static final String ANSI_B_GREEN = "\u001B[42m";
  public static final String ANSI_B_YELLOW = "\u001B[43m";
  public static final String ANSI_B_BLUE = "\u001B[44m";
  public static final String ANSI_B_PURPLE = "\u001B[45m";
  public static final String ANSI_B_CYAN = "\u001B[46m";
  public static final String ANSI_B_WHITE = "\u001B[47m";
}
