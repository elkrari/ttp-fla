package utils;

/**
 * swap two nodes
 * 
 * @author kyu
 *
 */
public class InsertionHelper {

  /**
   * Insert a city existing in the position i to a new position j
   * 
   * @param tour TSP tour
   * @param i old position
   * @param j new position
   */
  public static void doInsertion(int[] tour, int i, int j) {

    int tmp = tour[i];
    if(i<j)
        for(int k=i;k<j;k++)
            tour[k]=tour[k+1];
    else
        for(int k=i;k>j;k--)
            tour[k]=tour[k-1];
    tour[j]=tmp;
  }

}
