package utils;

/**
 * 2-OPT operations
 * 
 * @author kyu
 *
 */
public class ThreeOptHelper {


  /**
   * do 3Opt arcs exchange
   *
   */
  public static int[] do3Opt(int[] tour, int[] pos, int i, int j, int k, int option) {
    int x,n=tour.length,z=0;
    int[] Sn = new int[n];
    if (option == 1) {
      for (x = i + 1; x <= j; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = k + 1; x < n; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = 0; x <= i; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = j + 1; x <= k; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
    } else if (option == 2) {
      for (x = i + 1; x <= j; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = k + 1; x < n; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = 0; x <= i; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = k; x >= j + 1; x--) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
    } else if (option == 3) {
      for (x = i + 1; x <= j; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = i; x >= 0; x--) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = n - 1; x >= k + 1; x--) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = j + 1; x <= k; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
    } else {
      for (x = i + 1; x <= j; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = k; x >= j + 1; x--) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = i; x >= 0; x--) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = n - 1; x >= k + 1; x--) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
    }
    return Sn;
  }

  /**
   * do 3Opt arcs exchange
   *
   */
  public static int[] do3Opt(int[] tour, int i, int j, int k, int option) {
    int x,n=tour.length,z=0;
    int[] pos = new int[n+1];
    int[] Sn = new int[n];
    for (int q=0; q<n; q++) {
      pos[tour[q]] = q;
    }
    if (option == 1) {
      for (x = i + 1; x <= j; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = k + 1; x < n; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = 0; x <= i; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = j + 1; x <= k; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
    } else if (option == 2) {
      for (x = i + 1; x <= j; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = k + 1; x < n; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = 0; x <= i; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = k; x >= j + 1; x--) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
    } else if (option == 3) {
      for (x = i + 1; x <= j; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = i; x >= 0; x--) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = n - 1; x >= k + 1; x--) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = j + 1; x <= k; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
    } else {
      for (x = i + 1; x <= j; x++) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = k; x >= j + 1; x--) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = i; x >= 0; x--) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
      for (x = n - 1; x >= k + 1; x--) {
        pos[tour[x]]=z;
        Sn[z++] = (tour[x]);
      }
    }
    return Sn;
  }

}
