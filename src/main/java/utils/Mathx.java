package utils;

/**
 * Created by yafrani on 8/3/17.
 */
public class Mathx {
  public static int max(int[] tab) {
    int max = tab[1];
    for (int i=1; i<tab.length; i++) {
      if (tab[i] > max) {
        max = tab[i];
      }
    }
    return max;
  }

  public static int max(int[] tab, int[] idx) {
    int max = tab[1];
    idx[0] = 1;
    for (int i=1; i < tab.length; i++) {
      if (tab[i] > max) {
        max = tab[i];
        idx[0] = i;
      }
    }
    return max;
  }

  public static int min(int[] tab) {
    int min = tab[1];
    for (int i=1; i<tab.length; i++) {
      if (tab[i] < min) {
        min = tab[i];
      }
    }
    return min;
  }

  public static int min(int[] tab, int[] idx) {
    int min = tab[1];
    idx[0] = 1;
    for (int i=1; i < tab.length; i++) {
      if (tab[i] < min) {
        min = tab[i];
        idx[0] = i;
      }
    }
    return min;
  }

  public static int[] zeros(int n) {
    int[] tab = new int[n];
    for (int i=0; i < tab.length; i++) tab[i] = 0;
    return tab;
  }

  public static int[] ones(int n) {
    int[] tab = new int[n];
    for (int i=0; i < tab.length; i++) tab[i] = 1;
    return tab;
  }

  public static int[][] ones(int m, int n) {
    int[][] tab = new int[m][n];
    for (int i=0; i < tab.length; i++)
      for (int j=0; j < tab[i].length; j++)
        tab[i][j] = 1;
    return tab;
  }

  public static int sum(int tab[]) {
    int sum = 0;
    for (int i=1; i < tab.length; i++) sum += tab[i];
    return sum;
  }
}
