package generator;

/**
 * @elkrari
 * 12/06/2017
 */

import enumerations.BitstringGen;
import enumerations.PermGen;
import ttp.TTP1Instance;
import ttp.TTPSolution;
import utils.CityCoordinates;

import java.util.ArrayList;
import java.util.Arrays;

import static utils.RandGen.randStr;

public class TTP1Generator {


    /**
     * Generate a random TTP1 instance fom the given parameters.
     * Instance name is generated randomly.
     *
     * @param nbCities {@code Integer} - Number of cities
     * @param tspWidth {@code Double} - TSP's area width
     * @param tspHeight {@code Double} - TSP's area height
     * @param knapsackDataType {@code String} - Knapsack Data Type:
     *                                       <code>"u"</code> for uncorrelated,
     *                                       <code>"usw"</code> for uncorrelated with similar weights,
     *                                       <code>"bsc"</code> for bounded strongly correlated
     * @param nbItemsPerCity {@code Integer} - Number of items per city (We suppose that cities have the same number of objects)
     * @param capacityCategory {@code Integer} - Knapsack capacity category, ranged from 1 to 10
     *
     * @return A new random {@code TTP1Instance}
     *
     */
    public TTP1Instance generate(int nbCities, double tspWidth, double tspHeight,
                                 String knapsackDataType, int nbItemsPerCity, int capacityCategory){

        TTP1Instance ttp1 = new TTP1Instance("eil"+nbCities+"_n"+(nbCities-1)*nbItemsPerCity+"_bounded-strongly-corr_05.ttp"); //setRandomTSP(new TTP1Instance(),nbCities,tspWidth,tspHeight);

        ttp1 = setRandomKP(ttp1,knapsackDataType,nbItemsPerCity,nbCities,capacityCategory);

        //-----PROBLEM NAME-----
        ttp1.setName("gen_m"+nbCities+
          "-F"+nbItemsPerCity+
          "-T"+knapsackDataType+
          "-C"+capacityCategory+
          "_"+System.currentTimeMillis());

        //-----MIN SPEED-----
        ttp1.setMinSpeed(0.1);

        //-----MAX SPEED-----
        ttp1.setMaxSpeed(1);

        //-----RENTING RATIO-----
        ttp1.setRent(setRentingRate(ttp1));

        return ttp1;
    }

    /**
     * Generate a random TTP1 instance fom the given parameters, including instance name and Renting Rate.
     *
     * @param name {@code String} - Instance name
     * @param nbCities {@code Integer} - Number of cities
     * @param tspWidth {@code Double} - TSP's area width
     * @param tspHeight {@code Double} - TSP's area height
     * @param knapsackDataType {@code String} - Knapsack Data Type:
     *                                       <code>"u"</code> for uncorrelated,
     *                                       <code>"usw"</code> for uncorrelated with similar weights,
     *                                       <code>"bsc"</code> for bounded strongly correlated
     * @param nbItemsPerCity {@code Integer} - Number of items per city (We suppose that cities have the same number of objects)
     * @param capacityCategory {@code Integer} - Knapsack capacity category, ranged from 1 to 10
     * @param rent {@code Double} - Renting Ratio
     *
     * @return A new random {@code TTP1Instance}
     *
     */
    public TTP1Instance generate(String name, int nbCities, double tspWidth, double tspHeight,
                                 String knapsackDataType, int nbItemsPerCity, int capacityCategory,
                                 double rent){

        TTP1Instance ttp1 = setRandomTSP(new TTP1Instance(),nbCities,tspWidth,tspHeight);

        ttp1 = setRandomKP(ttp1,knapsackDataType,nbItemsPerCity,nbCities,capacityCategory);

        //-----PROBLEM NAME-----
        ttp1.setName(name);

        //-----MIN SPEED-----
        ttp1.setMinSpeed(0.1);

        //-----MAX SPEED-----
        ttp1.setMaxSpeed(1);

        //-----RENTING RATIO-----
        ttp1.setRent(rent);

        return ttp1;
    }


    /**
     * Generate a random TTP1 instance with the main parameters.
     *
     * @param nbCities {@code Integer} - Number of cities
     * @param knapsackDataType {@code String} - Knapsack Data Type:
     *                                       <code>"u"</code> for uncorrelated,
     *                                       <code>"usw"</code> for uncorrelated with similar weights,
     *                                       <code>"bsc"</code> for bounded strongly correlated
     * @param nbItemsPerCity {@code Integer} - Number of items per city (We suppose that cities have the same number of objects)
     * @param capacityCategory {@code Integer} - Knapsack capacity category, ranged from 1 to 10
     *
     * @return A new random {@code TTP1Instance}
     *
     */
    public TTP1Instance generate(int nbCities,
                                 String knapsackDataType, int nbItemsPerCity, int capacityCategory){

        TTP1Instance ttp1 = setRandomTSP(new TTP1Instance(),nbCities,nbCities*50,nbCities*50);

        ttp1 = setRandomKP(ttp1,knapsackDataType,nbItemsPerCity,nbCities,capacityCategory);

        //-----PROBLEM NAME-----
        ttp1.setName(randStr(6));

        //-----MIN SPEED-----
        ttp1.setMinSpeed(0.1);

        //-----MAX SPEED-----
        ttp1.setMaxSpeed(1);

        //-----RENTING RATIO-----
        ttp1.setRent(setRentingRate(ttp1));

        return ttp1;
    }



    public TTP1Instance setRandomKP(TTP1Instance ttp1, String knapsackDataType, int nbItemsPerCity, int nbCities, int capacityCategory){

        //-----KNAPSACK DATA TYPE-----
        if(knapsackDataType.equals("u"))
            ttp1.setKnapsackDataType("uncorrelated");
        else if(knapsackDataType.equals("usw"))
            ttp1.setKnapsackDataType("uncorrelated, similar weights");
        else if(knapsackDataType.equals("bsc"))
            ttp1.setKnapsackDataType("bounded strongly corr");

        //-----NUMBER OF ITEMS-----
        int nbItems = (nbCities-1) * nbItemsPerCity;
        ttp1.setNbItems(nbItems);

        //-----ITEMS SECTION-----
        ttp1.setProfits(new int[nbItems]);
        ttp1.setWeights(new int[nbItems]);
        ttp1.setAvailability(new int[nbItems]);

        int index;
        for(int i=1;i <= nbItemsPerCity;i++){
            for(int c=2;c <= nbCities;c++){
                index = nbCities*(i-1)+c-2;
                if(knapsackDataType.compareTo("u")==0){
                    ttp1.getAvailability()[index] = c;
                    do{
                        ttp1.getProfits()[index]=(int)(Math.random() * 1000)+1;
                        ttp1.getWeights()[index]=(int)(Math.random() * 1000)+1;
                    }while(i!=1&&c!=2&&
                            (contains(ttp1.getProfits(),ttp1.getProfits()[index],index-1)||
                                    contains(ttp1.getWeights(),ttp1.getWeights()[index],index-1)));

                }else if(knapsackDataType.compareTo("usw")==0){
                    ttp1.getAvailability()[index] = c;
                    do{
                        ttp1.getProfits()[index]=(int)(Math.random() * 11)+1000;
                        ttp1.getWeights()[index]=(int)(Math.random() * 11)+1000;
                    }while(i!=1&&c!=2&&
                            (contains(ttp1.getProfits(),ttp1.getProfits()[index],index-1)||
                                    contains(ttp1.getWeights(),ttp1.getWeights()[index],index-1)));

                }else if(knapsackDataType.compareTo("bsc")==0){
                    ttp1.getAvailability()[index] = c;
                    do{
                        ttp1.getWeights()[index]=(int)(Math.random() * 1000)+1;
                        ttp1.getProfits()[index]=ttp1.getWeights()[index]+100;
                    }while(i!=1&&c!=2&&
                            (contains(ttp1.getProfits(),ttp1.getProfits()[index],index-1)||
                                    contains(ttp1.getWeights(),ttp1.getWeights()[index],index-1)));

                }

            }
        }

        //-----CAPACITY OF KNAPSACK-----
        ttp1.setCapacity(setCapacity(capacityCategory,ttp1.getWeights()));

        ttp1.clusterItems();

        return ttp1;
    }

    public TTP1Instance setRandomTSP(TTP1Instance ttp1, int nbCities, double tspWidth, double tspHeight){

        //-----EDGE_WEIGHT_TYPE-----
        ttp1.setEdgeWeightType("CEIL_2D");

        //-----DIMENSION-----
        ttp1.setNbCities(nbCities);

        //-----Generate TSP Coordinates-----
        CityCoordinates[] coordinates = new CityCoordinates[nbCities];
        CityCoordinates cc;
        for(int i=0;i<nbCities;i++){
            do{
                cc = new CityCoordinates(Math.random() * tspWidth,Math.random() * tspHeight);
            }while(i!=0 && exist(coordinates,cc,i-1));
            coordinates[i] = cc;
        }
        ttp1.setCoordinates(coordinates);

        //-----DISTANCE MATRIX-----
        ttp1.setDist(new long[nbCities][nbCities]);
        for (int i = 0; i < nbCities; i++) {
            for (int j = 0; j < nbCities; j++)
                ttp1.getDist()[i][j] = (long)Math.ceil(ttp1.getCoordinates()[i].distanceEuclid(ttp1.getCoordinates()[j]));
        }

        return ttp1;
    }

    private long setCapacity(int capacityCategory, int[] weights){
        long C = 0;
        int l = weights.length;
        for(int i=0;i<l;i++)
            C+=weights[i];

        return C*capacityCategory/11;
    }

    private  double setRentingRate(TTP1Instance ttp) {
        /*----- Best Picking Plan -----*/
        int n = ttp.getNbItems();
        long profit, bestProfit = 0;
        int[] pickingPlan;
        String bestPick = "1234";
        BitstringGen bitstringGen = new BitstringGen();
        bitstringGen.generate(n);
        for (String str : bitstringGen.bitstrings) {
            pickingPlan = setPickingPlan(ttp, str);
            profit = getKpProfit(ttp, pickingPlan);
            if (profit > bestProfit & getKpWeight(ttp,pickingPlan) <= ttp.getCapacity()) {
                bestProfit = profit;
                bestPick = str;
            }
        }

        /*----- Best Tour Time according to bestPick -----*/
        int m = ttp.getNbCities();
        double tourTime, bestFt = Double.MAX_VALUE;
        PermGen permGen = new PermGen();
        permGen.generate(m);
        for (ArrayList<Integer> perm:permGen.perms) {
            tourTime = setTourTime(ttp,setPickingPlan(ttp,bestPick),perm);
            if(tourTime < bestFt)
                bestFt= tourTime;

        }

        return bestProfit /bestFt;
    }

    private double setTourTime(TTP1Instance ttp,int[] pickPlan, ArrayList<Integer> tour){
        double C = (ttp.getMaxSpeed()-ttp.getMinSpeed())/ttp.getCapacity(); // velocity const
        double velocity;

        long acc;       // iteration weight accumulator
        long wc = 0;    // current weight
        double ft = 0;  // tour time

        // visit all cities
        for (int i=0; i<ttp.getNbCities(); i++) {
            acc = 0;
            // check only items contained in current city
            for (int j : ttp.getClusters()[i])
                if (pickPlan[j]!=0)
                    acc += ttp.getWeights()[j];

            wc += acc;
            velocity = ttp.getMaxSpeed() - wc*C;

            int h = (i+1)% ttp.getNbCities();
            ft += ttp.distFor(tour.get(i)-1,tour.get(h)-1) / velocity;
        }

        return ft;
    }

    private int[] setPickingPlan(TTP1Instance ttp, String str){
        int[] pickingPlan = new int[str.length()];
        for (int i=0; i<str.length(); i++) {
            if (str.charAt(i)=='0')
                pickingPlan[i] = 0;
            else
                pickingPlan[i] = ttp.getAvailability()[i];
        }
        return pickingPlan;
    }

    private long getKpProfit(TTP1Instance ttp, int[] pickingPlan){
        long fp =0;

        // visit all cities
        for (int i=0; i<ttp.getNbCities(); i++)
            // check only items contained in current city
            for (int j : ttp.getClusters()[i])
                if (pickingPlan[j] != 0)
                    fp += ttp.getProfits()[j];

        return fp;
    }

    private long getKpWeight(TTP1Instance ttp, int[] pickingPlan){
        long w =0;

        // visit all cities
        for (int i=0; i<ttp.getNbCities(); i++)
            // check only items contained in current city
            for (int j : ttp.getClusters()[i])
                if (pickingPlan[j] != 0)
                    w += ttp.getWeights()[j];

        return w;
    }

    private boolean exist(CityCoordinates[] setCoordinates, CityCoordinates coordinates, int limit){
        for(int i=0;i<=limit;i++){
            if(setCoordinates[i].compare(coordinates))
                return true;
        }
        return false;
    }

    private boolean contains(int[] array, int element, int limit){
        for(int i=0;i<=limit;i++){
            if(array[i] == element)
                return true;
        }
        return false;
    }


    /*private  double setRentingRateOld(TTP1Instance ttp){
        int m = ttp.getNbCities();
        int n = ttp.getNbItems();
        long bestFp = 0;
        double bestFt = 0;

        PermGen permGen = new PermGen();
        permGen.generate(m);

        for (ArrayList<Integer> perm:permGen.perms) {
            BitstringGen bitstringGen = new BitstringGen();
            bitstringGen.generate(n);
            for (String str:bitstringGen.bitstrings) {
                TTPSolution sol = new TTPSolution(perm, str, ttp);
                ttp.objective(sol);
                if (sol.fp > bestFp){
                    bestFp = sol.fp;
                    bestFt = sol.ft;
                }else if(sol.fp == bestFp && sol.ft < bestFt)
                    bestFt = sol.ft;
            }
        }

        return bestFp/bestFt;
    }*/
}
