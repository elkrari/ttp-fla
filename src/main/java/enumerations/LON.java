package enumerations;

import solver.LocalSearch;
import ttp.TTP1Instance;
import ttp.TTPSolution;
import utils.Deb;

import java.io.*;
import java.util.*;

/**
 * Created by yafrani on 11/6/17.
 */
public class LON {

  // basins of attraction
  public Map<Long, LinkedHashSet<SolutionIndex>> basins;

  // LON adjacency list
  public Map<Long, TreeSet<Long>> adjList;

  // neighborhood matrix
  public ArrayList<BitSet> neighborhood;

  TTP1Instance ttp;
  LinkedList<TTPSolution> solutions;
  LocalSearch ls;



  public LON(TTP1Instance ttp, LinkedList<TTPSolution> solutions, LocalSearch ls) {

    this.ttp = ttp;
    this.solutions = solutions;

    this.ls = ls;
    this.ls.bestfit();
    this.ls.noDebug();

    this.basins = new HashMap<>();

    //this.basinsIndices = new ArrayList<>();

    this.adjList = new HashMap<>();

    int N = solutions.size();
    this.neighborhood = new ArrayList<>(N);
    for (int i=0; i<N; i++) {
      this.neighborhood.add(i, new BitSet(i));
    }
  }




  /**
   * matrix containing the neighborhood relationship
   */
  public void loadNeighborhood() {

    int N = solutions.size();

    for (int i=0; i<N; i++) {

      // current solution
      TTPSolution si = solutions.get(i);

      // si neighbors
      ArrayList<TTPSolution> Nsi = ls.neighborhood(si);

      // si and its neighbors in the neighborhood matrix
      for (TTPSolution nsi:Nsi) {
        int j = nsi.index;
        int k,p;
        if (i<j) {
          k=j; p=i;
        }
        else {
          if(i>j) {
            k=i; p=j;
          }
          else {
            continue;
          }
        }
        // get current column
        BitSet col = neighborhood.get(k);
        // set bit
        col.set(p, true);
      }

    }

//    for (int i=0;i<N; i++) {
//      Deb.echo(i+">>"+neighborhood.get(i));
//    }
  }


  /**
   * load basins with solutions
   */
  public void loadBasins() {
    TTPSolution lo, sol;
    int nbSolutions = this.solutions.size();

    for (int i=0; i<nbSolutions; i++) {
      sol = this.solutions.get(i);

      // eliminate non-feasible solutions
      if (!sol.feasible) continue;

      this.ls.setS0(sol);
      lo = this.ls.search();
      ttp.objective(lo);

      // eliminate "non-feasible local optima"
      if (!lo.feasible) continue;

      long loCmp = Long.parseLong(lo.xCompressedString());
      long solCmp = Long.parseLong(sol.xCompressedString());
      SolutionIndex si = new SolutionIndex(solCmp,sol.index);
      LinkedHashSet<SolutionIndex> basinSolutions = this.basins.get(loCmp);
      if (basinSolutions==null) {
        LinkedHashSet<SolutionIndex> solSet = new LinkedHashSet<>();
        solSet.add(si);
        this.basins.put(loCmp, solSet);
      }
      else {
        basinSolutions.add(si);
      }
    }
  }


  public void connectBasins() {
    int nbNodes = basins.size();
    ArrayList<Long> nodes = new ArrayList<>(basins.keySet());
    for (Long n:nodes) {
      adjList.put(n, new TreeSet<>());
    }

    // see of node i is connected to node j
    for (int i=0; i<nbNodes; i++) {
      for (int j=0; j<nbNodes; j++) {

        if (j==i) continue;

        long loi = nodes.get(i);
        long loj = nodes.get(j);

        if (adjList.get(loi).contains(loj)) {
          adjList.get(loj).add(loi);
          continue;
        }

        // get solutions inside basins
        LinkedHashSet<SolutionIndex> bi = basins.get(loi);
        LinkedHashSet<SolutionIndex> bj = basins.get(loj);
        boolean foundIt = false;

        for (SolutionIndex xi:bi) {
          for (SolutionIndex xj:bj) {
            // check if basin of J contains a neighbor in I
            if (areNeighbors(xi.index, xj.index)) {
              foundIt = true;
              break;
            }
          }
          if (foundIt) break;
        }

        if (foundIt) {
          adjList.get(loi).add(loj);
        }

      }
    }
  }


  // i and j are solution indices
  public  boolean areNeighbors(int i, int j) {
    return neighborhood.get(i).get(j) || neighborhood.get(j).get(i);
  }


  @Override
  public String toString() {

    String s = "";
    Set<Long> keySet = basins.keySet();

    s += ">> Attraction basins:\n";
    if (keySet.size()>100) {
      s += "/!\\ basins' size too large.";
      return s;
    }

    for (long key : keySet) {
      int size = basins.get(key).size();
      TTPSolution lo = new TTPSolution(ttp.getNbCities(), ttp.getNbItems(), key);
      ttp.objective(lo);

      if (!lo.feasible) s+="N";

      s += key + " ("+lo.fitness+"|"+size+"): ";
      for (SolutionIndex x:basins.get(key)) {
        if (!solutions.get(x.index).feasible) s+="N";
        s += x.solution + ", ";
      }
      s += "\n";
    }

    s += "\n>> Local optima net:\n";
    Set<Long> nodes = adjList.keySet();
    for (long node : nodes) {
      int degree = adjList.get(node).size();
      TTPSolution lo = new TTPSolution(ttp.getNbCities(), ttp.getNbItems(), node);
      ttp.objective(lo);

      if (!lo.feasible) s+="N";

      s += node + " ("+lo.fitness+"|"+degree+"): " + adjList.get(node)+"\n";
    }
    return s;
  }


  /**
   * write the local optima network into a text file
   */
  public void store(String filename) throws FileNotFoundException {
    int nbCities = ttp.getNbCities();
    int nbItems = ttp.getNbItems();

    PrintWriter output = new PrintWriter(filename);

    output.print("LOCAL OPTIMA NET:\n");

    Set<Long> nodes = adjList.keySet();
    for (long node : nodes) {
      int size = basins.get(node).size();
      int degree = adjList.get(node).size();
      TTPSolution lo = new TTPSolution(ttp.getNbCities(), ttp.getNbItems(), node);
      ttp.objective(lo);
      output.print(node + " ("+lo.fitness+"|"+degree+"|"+size+"): [");
      for (Long x:adjList.get(node)) {
        TTPSolution xs = new TTPSolution(nbCities, nbItems, x);
        ttp.objective(xs);

        if (!xs.feasible) output.print("N");

        output.print(x + ",");
      }
      output.println("]");
    }
    output.close();
  }

  /**
   * write the local optima network into a text file
   */
  public void storeBasins(String filename) throws FileNotFoundException {

    PrintWriter output = new PrintWriter(filename);

    output.print("BASINS OF ATTRACTION:\n");

    Set<Long> keySet = basins.keySet();

    for (long key : keySet) {
      int size = basins.get(key).size();
      TTPSolution lo = new TTPSolution(ttp.getNbCities(), ttp.getNbItems(), key);
      ttp.objective(lo);

      output.print(key + " ("+lo.fitness+"|"+size+"): ");
      for (SolutionIndex x:basins.get(key)) {
        if (!solutions.get(x.index).feasible)
          output.print("N");
        output.print(x.solution + ", ");
      }
      output.println();
    }

    output.close();
  }




  public class SolutionIndex {
    public long solution;
    public int index;
    public SolutionIndex(long s, int i) {
      solution = s;
      index = i;
    }
  }

}
