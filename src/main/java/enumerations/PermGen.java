package enumerations;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yafrani on 8/19/17.
 */
public class PermGen {

  public ArrayList< ArrayList<Integer> > perms;

  public PermGen() {
    this.perms = new ArrayList<>();
  }

  public void generate(int len) {
    List<Integer> arr = new ArrayList<>();
    for (int i=1; i<=len; i++) arr.add(i);

    permutations(new ArrayList<>(0), arr);
  }

  private void permutations(ArrayList<Integer> prefix, List<Integer> arr) {
    int n = arr.size();

    if (n==0 && prefix.get(0)!=1) return;

    if (n == 0) {
      //Deb.echol(">>>");Deb.echo(prefix);
      perms.add(prefix);
    }
    else {
      for (int i = 0; i < n; i++) {
        ArrayList<Integer> newPrefix = new ArrayList<>();
        newPrefix.addAll(prefix);
        newPrefix.add(arr.get(i));

        ArrayList<Integer> p1 = new ArrayList<>(arr.subList(0, i));
        ArrayList<Integer> p2 = new ArrayList<>(arr.subList(i + 1, n));

        p1.addAll(p2);

        permutations(newPrefix, p1);
      }
    }
  }

}
