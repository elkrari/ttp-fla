package enumerations;

import java.util.ArrayList;

/**
 * Created by yafrani on 10/21/17.
 */
public class BitstringGen {

  public ArrayList<String> bitstrings;

  public BitstringGen() {
    this.bitstrings = new ArrayList<>();
  }

  public void generate(int len) {
    getBinaries(len, "");
  }

  private ArrayList<String> getBinaries(int len, String current) {
    ArrayList<String> binaries = new ArrayList<>();

    if (current.length() == len) {
      binaries.add(current);
      return binaries;
    }

    //pad a 0 and 1 in front of current;
    binaries.addAll(getBinaries(len, "0" + current));
    binaries.addAll(getBinaries(len, "1" + current));

    bitstrings = binaries;

    return binaries;
  }

}
