package enumerations;

import ttp.TTP1Instance;
import ttp.TTPSolution;
import utils.Deb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by yafrani on 10/21/17.
 */
public class TTPEnumerator {

  // all possible solutions
  public LinkedList<TTPSolution> solutions = new LinkedList<>();

  // all possible solutions
  public HashMap<Long, Integer> indices = new HashMap<>();

  public void enumerate (TTP1Instance ttp) {
    int m = ttp.getNbCities();
    int n = ttp.getNbItems();

    PermGen permGen = new PermGen();
    permGen.generate(m);
    int k = 0;
    for (ArrayList<Integer> perm:permGen.perms) {
      BitstringGen bitstringGen = new BitstringGen();
      bitstringGen.generate(n);
      for (String str:bitstringGen.bitstrings) {
        TTPSolution sol = new TTPSolution(perm, str, ttp);
        ttp.objective(sol);
        sol.index = k++;
        // add solution to list
        this.solutions.add(sol);
        // map indices
        long cmpSol = Long.parseLong(sol.xCompressedString());
        this.indices.put(cmpSol, sol.index);
      }
    }
  }


  // fine as long as m,n<10
  @Override
  public String toString() {
    String s = "";
    for (TTPSolution sol: solutions) {
      s += sol.compressedString()+" "+sol.index+"\n";
    }
    return s;
  }
}
